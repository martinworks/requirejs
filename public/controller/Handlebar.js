require([], function(){

    require([
        "jquery",
        "$SelectBox",
        "css!../css/tab"
    ], function($, SelectBox) {
        new SelectBox({
            "wrap" : "[data-module-pop='wrap']",
            "btn": "button",
            "selectListWrap" : "ul",
            "selectList" : "li a",
            "mobileSelectWrap" : "select"
        }).divideEvents();
    });

})
