define([], function(){
    requirejs([
        "jquery",
        "selectBox",
        "text!module/selectbox.html",
        "css!module/selectbox"
    ], function($, SelectBox,text) {
        $('document,body').append(text);
        new SelectBox({
            "wrap" : "[data-module-pop='wrap']",
            "btn": "button",
            "selectListWrap" : "ul",
            "selectList" : "li a",
            "mobileSelectWrap" : "select"
        }).divideEvents();
    });

})
