require([], function(){
    require([
        "jquery",
        "tab",
        "text!module/tab.html",
        "css!module/tab"
    ],function($, tab, text){
        $('document,body').append(text);
        tab.init({
            "wrapper":"[data-module-tab='wrap']",
            "dataWrapper":"[data-ui-work='wrap']",
            "btnWrap":"[data-module-tabBtn='wrap']",
            "btnList":"[data-module-tab='contentsList']",
            "btn" : ".module_tab__button",
            "tabList" : "[data-module-tab='contentsList']",
            "tabContents" : ".module_tab__contents"
        });
    });
});
