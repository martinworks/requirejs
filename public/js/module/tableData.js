define([
	'jquery'
    ,'handlebars'
],function ($,Handlebars) {
    var getData=(function(){
        function init(options){


            getData(options);
        }

        function setSelector(selector){
            return {
                "wrapper" : $(selector.wrapper),
                "bindWrap" : $(selector.bindWrap)
            }
        }

        function getData(selector){
            var bindWrap =setSelector(selector).bindWrap;
            var wrapper = setSelector(selector).wrapper;
            var dummy = wrapper.html();
			var template = Handlebars.compile(dummy);
            $.ajax({
				type :"GET",
				url : "http://localhost:3000/persons",
				dataType : "json",
				cache : false,
				success : function(data){
                    var item = template(data);
                    for (i in data){
                        var item = template(data[i]);
                        bindWrap.append(item);
                        //console.log(data[i])
                    }
                    console.log(item)

				},
				error : function(){
					alert("데이터를 불러올 수 없습니다.");
				}
			})
        }
        return {
            init : init
        }
    })();
    return getData
});
