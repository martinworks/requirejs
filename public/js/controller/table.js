define([], function(){
    requirejs([
        "jquery"
        ,"tableData"
        ,"text!module/table.html"
        ,"css!module/table.css"
    ], function($,data,text) {
        $('document,body').append(text);
        data.init({
            "wrapper" : "#getDataContents",
            "bindWrap" : "[data-bind='wrap']"
        })
    });

})
