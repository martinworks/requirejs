define(['responsive'],function (SIZE){
	var DEVICE_TEMP = SIZE.getDeviceSize();
	var DEVICE = DEVICE_TEMP;
	$(window).resize( function(){
		DEVICE_TEMP = SIZE.getDeviceSize();
		if(DEVICE !==DEVICE_TEMP ){
			DEVICE = DEVICE_TEMP;
		}
	})

	var SelectBox = function(options){
		this.wrap =$(options.wrap);
		this.btn=$(options.btn, this.wrap);
		this.selectListWrap = $(options.selectListWrap, this.wrap);
		this.selectList = $(options.selectList, this.selectListWrap);
		this.mobileSelectWrap = $(options.mobileSelectWrap, this.wrap);
	};



	SelectBox.prototype = {
		divideEvents:function(){
			//Devide Device resolution
			var that = this;
			var NEW_SIZE=SIZE.getDeviceSize();
			$(window).resize(function(){
				OLD_SIZE = SIZE.getDeviceSize();
				if( OLD_SIZE !== NEW_SIZE ){
					NEW_SIZE = OLD_SIZE;
					that.bindEvents(NEW_SIZE);
				}
			})
			this.bindEvents(NEW_SIZE);
		},
		bindEvents:function(resolution) {
			var that =this;
			//PC Event
			this.btn.unbind("click").on("click", function(){
				resolution == "PC" ? that.pcToggle(): false;
			})
			this.selectList.on("mouseenter", function(event){
				resolution == "PC" ? that.pcHover($(this)): false;
			})
			this.selectList.on("click", function(){
				resolution == "PC" ? that.pcHover($(this), "clickEvt") : false;
			})
			//Mobile and Tablet Event
			resolution !== "PC" ? this.selectListWrap.hide() : false;
			this.mobileSelectWrap.unbind("change").on("change", function(){
				resolution !== "PC" ? that.mobileSelect( $(this)) : false;
			})
		},
		pcToggle:function(){
			if(this.selectListWrap.is(":visible")){
				this.selectListWrap.slideUp("fast");
			}else{
				this.selectListWrap.slideDown("fast");
			}
		},
		pcHover: function(me, evt){
			this.btn.text(me.text())
			evt=="clickEvt" ? this.pcToggle() : false;
		},
		mobileSelect:function(me){
			this.btn.text(me.find("option:selected").text());
		}
	};

	return SelectBox;
	}
);
