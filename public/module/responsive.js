define([
	'jquery'
	,'Device'
],function ($, device) {
	var currentSize= "";
	var resolution;
	var oldSize;
	var newSize;
	var windowSize = Math.max( document.documentElement.clientWidth | window.innerWidth | window.availWidth);
	function init(){
		windowSize = Math.max( document.documentElement.clientWidth | window.innerWidth | window.availWidth);
		checkSize();
		callSize();
	}
	init();
	function checkSize(){
		resolution = windowSize;
		if(resolution > device.TABLET_SIZE ){
			resolution = "PC";
		} else if ( (resolution <= device.TABLET_SIZE) && (resolution > device.MOBILE_SIZE) ) {
			resolution="Tablet";
		} else if ( resolution <= device.MOBILE_SIZE){
			resolution="Mobile";
		}
	}

	function callSize(){
		newSize = resolution;
		if(newSize !== oldSize){
			oldSize = newSize;
			currentSize = oldSize;
			//Check Device size;
			console.log(":::: Device Size Check : " + currentSize + " ::::");
		}
	}

	$(window).resize(function(){
		init();
	})
	return {
		getDeviceSize : function(){
			return currentSize;
		}
	}

});
