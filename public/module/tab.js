define([
	'jquery',
	'handlebars'
],function ($,Handlebars) {
	var tabFunc = (function(){
		var newTemp;
		var oldTemp=0;

		function init(options){
			bindEvents(options);
		}

		function setSelector(selector){
			return{
				wrapper : selector.wrapper,
				dataWrapper : selector.dataWrapper,
				btnWrap : selector.btnWrap,
				btnList : selector.btnList,
				btn : selector.btn,
				tabList : selector.tabList,
				tabContents : selector.tabContents
			}
		}

		function bindEvents(selector){
			var btn = $(selector.btn);
			btn.on("click", function(){
				var value = $(this).data("tab-value");
				var outputData =getDataValue(selector,value);
				activeTab($(this),selector);
			});
		}


		function getDataValue(selector,value){
			var workDataWrap = $(selector.dataWrapper);
			var wrapper = $(selector.wrapper);
			var contentsWrapper = $(selector.tabContents);
			var dummy = workDataWrap.html();
			var template = Handlebars.compile(dummy);

			contentsWrapper ? contentsWrapper.remove() : false;
			$.ajax({
				type :"GET",
				url : "js/module/data.json",
				dataType : "json",
				cache : false,
				success : function(data){
					var item = template(data.fruit[returnValue(data,value)]);
					wrapper.append(item);
				},
				error : function(){
					alert("데이터를 불러올 수 없습니다.");
				}
			})
		}

		function returnValue(data,value){
			for(var i=0; i<data.fruit.length;i++){
				if(value == data.fruit[i].value){
					return i;
				}
			}
		}

		function activeTab(me, selector){
			var btnList = $(selector.btnList);
			var tabList = $(selector.tabList);
			newTemp = me.parent().index();
			btnList.eq(oldTemp).removeClass("on");
			btnList.eq(newTemp).addClass("on");
			tabList.eq(oldTemp).removeClass("on");
			tabList.eq(newTemp).addClass("on");
			oldTemp = newTemp;
		}

		return{
			init : init
		}
	}());

	return tabFunc;

});
